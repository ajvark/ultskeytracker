﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace UltsKeyTracker
{
    public partial class Form1 : Form
    {
        Reader KeyReader;
        DataTable keyTable;
        public Form1()
        {
            InitializeComponent();
            KeyReader = new Reader(); //initializing the keyreader
            Resize += new EventHandler(Form1_Resize); //initializing the resize event so that datagridview will resize accordingly
        }
        /// <summary>
        /// resizing the datagridview depending on how the form is resized
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Form1_Resize(object sender, EventArgs e)
        {
            this.dataGridView1.Size = new Size(Size.Width - 34, Size.Height - 92);
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// this converts a list into a datatable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items">the list we will be converting into a datatable</param>
        /// <returns></returns>
        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name, type);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }
        /// <summary>
        /// The New/Add button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog fd = new OpenFileDialog();
            fd.Filter = ".txt Files|*.txt";
            DialogResult result = fd.ShowDialog(); // Show the dialog.
            if (result == DialogResult.OK) // Test result.
            {
                try
                {
                    KeyReader.AddKeys(fd.FileName); //adding the keys from the file
                    keyTable = ToDataTable<Key>(KeyReader.keys); //converting the keys to a datatable
                    this.dataGridView1.DataSource = keyTable; //making the datatable the source of the datagridview
                    for (int i = 0; i < keyTable.Rows.Count; i++) //going through the all the rows and turning certain columns of cells into combo boxes
                    {
                        DataGridViewComboBoxCell c = new DataGridViewComboBoxCell();
                        c.Items.Add("Blog");
                        c.Items.Add("Curator");
                        c.Items.Add("Distribute");
                        c.Items.Add("Developers");
                        c.Items.Add("Friends/Family");
                        c.Items.Add("Let's Play");
                        c.Items.Add("LiveStream");
                        c.Items.Add("Media");
                        c.Items.Add("Mixer");
                        c.Items.Add("News");
                        c.Items.Add("Other");
                        c.Items.Add("Press");
                        c.Items.Add("Review");
                        c.Items.Add("SmashCast");
                        c.Items.Add("Social");
                        c.Items.Add("Stream");
                        c.Items.Add("Stream/Video");
                        c.Items.Add("Testing");
                        c.Items.Add("Text");
                        c.Items.Add("Text Review");
                        c.Items.Add("Twitch");
                        c.Items.Add("Unknown");
                        c.Items.Add("Video");
                        c.Items.Add("Video Review");
                        c.Items.Add("YouTube");
                        c.Value = this.dataGridView1[2, i].Value != null ? this.dataGridView1[2, i].Value : "Unknown";
                        this.dataGridView1[2, i] = c;
                        DataGridViewComboBoxCell c2 = new DataGridViewComboBoxCell();
                        c2.Items.Add("Stored");
                        c2.Items.Add("Sent");
                        c2.Items.Add("On-Site");
                        c2.Items.Add("Used");
                        c2.Items.Add("Rejected");
                        c2.Items.Add("Unknown");
                        c2.Value = this.dataGridView1[3, i].Value != null ? this.dataGridView1[3, i].Value : "Unknown";
                        this.dataGridView1[3, i] = c2;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }

        }
        /// <summary>
        /// The Open button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog fd = new OpenFileDialog();
            fd.Filter = ".csv Files|*.csv"; //here we look for a csv that is already opened
            DialogResult result = fd.ShowDialog(); // Show the dialog.
            if (result == DialogResult.OK) // Test result.
            {
                string file = fd.FileName;
                try
                {
                    
                    KeyReader.OpenKeys(fd.FileName); //we open the keys from the file
                    textBox1.Text = KeyReader.title; //we create the title accordingly
                    keyTable = ToDataTable<Key>(KeyReader.keys);
                    this.dataGridView1.DataSource = keyTable;
                    for(int i = 0; i < keyTable.Rows.Count; i++)
                    {
                        DataGridViewComboBoxCell c = new DataGridViewComboBoxCell();
                        c.Items.Add("Blog");
                        c.Items.Add("Curator");
                        c.Items.Add("Distribute");
                        c.Items.Add("Developers");
                        c.Items.Add("Friends/Family");
                        c.Items.Add("Let's Play");
                        c.Items.Add("LiveStream");
                        c.Items.Add("Media");
                        c.Items.Add("Mixer");
                        c.Items.Add("News");
                        c.Items.Add("Other");
                        c.Items.Add("Press");
                        c.Items.Add("Review");
                        c.Items.Add("SmashCast");
                        c.Items.Add("Social");
                        c.Items.Add("Stream");
                        c.Items.Add("Stream/Video");
                        c.Items.Add("Testing");
                        c.Items.Add("Text");
                        c.Items.Add("Text Review");
                        c.Items.Add("Twitch");
                        c.Items.Add("Unknown");
                        c.Items.Add("Video");
                        c.Items.Add("Video Review");
                        c.Items.Add("YouTube");
                        c.Value = this.dataGridView1[2, i];
                        this.dataGridView1[2, i] = c;
                        DataGridViewComboBoxCell c2 = new DataGridViewComboBoxCell();
                        c2.Items.Add("Stored");
                        c2.Items.Add("Sent");
                        c2.Items.Add("On-Site");
                        c2.Items.Add("Used");
                        c2.Items.Add("Rejected");
                        c2.Items.Add("Unknown");
                        c2.Value = this.dataGridView1[3, i];
                        this.dataGridView1[3, i] = c2;
                    }
                    
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }
        /// <summary>
        /// The Save Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder(); //we make a new stringbuilder

            IEnumerable<string> columnNames = keyTable.Columns.Cast<DataColumn>().
                                              Select(column => column.ColumnName);
            sb.AppendLine(string.Join(",", columnNames));

            foreach (DataRow row in keyTable.Rows)
            {
                IEnumerable<string> fields = row.ItemArray.Select(field =>
                  string.Concat("\"", field.ToString().Replace("\"", "\"\""), "\""));
                sb.AppendLine(string.Join(",", fields));
            } //we basically convert the datatable into a csv
            SaveFileDialog fd = new SaveFileDialog();
            fd.FileName = textBox1.Text;
            fd.Filter = ".csv Files|*.csv";
            DialogResult result = fd.ShowDialog(); // Show the dialog.
            if (result == DialogResult.OK) // Test result.
            {
                File.WriteAllText(fd.FileName, sb.ToString()); //we save the csv to a file
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }
        /// <summary>
        /// the credits button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Program by Ultdev (@ULTDEV)\nIcon by xylomon\nLicensed Under Creative Commons Attribution Share Alike (by-sa)");
        }
    }
}
