﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UltsKeyTracker
{
    class Key
    {
        private string KeyCode_; //the string of characters for the key
        private string Site_; //the site/channel it will go to
        private string Type_; //the type of media it will be represented as (let's play, review, etc.)
        private string Status_; //whether the key was sent, used, stored, rejected, etc.
        private bool PreLaunch_; //whether the key is meant to be given out pre-launch
        private DateTime DateGiven_; //the date the key was given on
        private string Who_; //the specific person the key was given to; sometimes separate from the site/channel
        public string KeyCode
        {
            get
            {
                return KeyCode_;
            }
            set
            {
                KeyCode_ = value;
            }
        }
        public string Site
        {
            get
            {
                return Site_;
            }
            set
            {
                Site_ = value;
            }
        }
        public string Type
        {
            get
            {
                return Type_;
            }
            set
            {
                Type_ = value;
            }
        }
        public string Status
        {
            get
            {
                return Status_;
            }
            set
            {
                Status_ = value;
            }
        }
        public bool PreLaunch
        {
            get
            {
                return PreLaunch_;
            }
            set
            {
                PreLaunch_ = value;
            }
        }
        public DateTime DateGiven
        {
            get
            {
                return DateGiven_;
            }
            set
            {
                DateGiven_ = value;
            }
        }
        public string Who
        {
            get
            {
                return Who_;
            }
            set
            {
                Who_ = value;
            }
        } 

        /// <summary>
        /// Constructor for if all key fields are filled
        /// </summary>
        /// <param name="kc">the key string</param>
        /// <param name="site">the site the key will go to</param>
        /// <param name="type">the type of coverage the key will be used for</param>
        /// <param name="status">the status of the key</param>
        /// <param name="p">whether the key is meant to be given out pre-launch</param>
        /// <param name="date">the date the key was given on</param>
        /// <param name="who">who specifically the key will go to</param>
        public Key(string kc, string site, string type, string status, bool p, DateTime date, string who)
        {
            KeyCode = kc;
            Site = site;
            Type = type;
            Status = status;
            PreLaunch = p;
            DateGiven = date;
            Who = who;
        }
        /// <summary>
        /// Constructor for if only the first four key fields are filled
        /// </summary>
        /// <param name="kc">the key string</param>
        /// <param name="site">the site the key will go to</param>
        /// <param name="type">the type of coverage the key will be used for</param>
        /// <param name="status">the status of the key</param>
        public Key(string kc, string site, string type, string status)
        {
            KeyCode = kc;
            Site = site;
            Type = type;
            Status = status;
        }
        /// <summary>
        /// constructor for when new keys are being added
        /// </summary>
        /// <param name="kc">the key string</param>
        public Key(string kc)
        {
            KeyCode = kc;
        }
    }
}
