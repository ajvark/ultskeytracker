﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Microsoft.VisualBasic.FileIO;

namespace UltsKeyTracker
{
    class Reader
    {
        public List<Key> keys;
        public string title;
        public Reader()
        {
            keys = new List<Key>();
        }
        /// <summary>
        /// opening a previously created key spreadsheet
        /// </summary>
        /// <param name="filename">the filename from which to open from</param>
        public void OpenKeys(string filename)
        {
            string[] keyList = File.ReadAllLines(filename);
            if (keyList[0].Split(',').Length == 1)
                title = keyList[0];
            else
                title = filename.Remove(filename.LastIndexOf('.')).Substring(filename.LastIndexOf('\\') + 1);
            for (int i = 1; i < keyList.Length; i++)
            {
                TextFieldParser parser = new TextFieldParser(new StringReader(keyList[i]));
                parser.HasFieldsEnclosedInQuotes = true;
                parser.SetDelimiters(",");
                while (!parser.EndOfData)
                {
                    string[] fields = parser.ReadFields();
                    if (fields.Length == 4)
                    keys.Add(new Key(fields[0], fields[1], fields[2], fields[3]));
                else
                    keys.Add(new Key(fields[0], fields[1], fields[2], fields[3], bool.Parse(fields[4]), DateTime.Parse(fields[5]), fields[6]));
                }

                parser.Close();
            }
        }
        /// <summary>
        /// adding keys from a new text file
        /// </summary>
        /// <param name="filename">the filename from which to add keys</param>
        public void AddKeys(string filename)
        {
            string[] keyList = File.ReadAllLines(filename);
            for (int i = 0; i < keyList.Length; i++)
            {
                keys.Add(new Key(keyList[i]));
            }
        }

    }
}
